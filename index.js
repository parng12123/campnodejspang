const env = require("dotenv").config({})
const express = require("express")
const app = express()

app.use(express.json({ limit: "50mb" }))       //เข้าผ่าน link
app.get("/api", (req, res) => {
   res.send("Hello world")
})
app.post("/name",(req,res)=>{
    res.send("post name")
})
app.delete("/name",(req,res)=>{
    res.send("delete name")
})
app.put("/name",(req,res)=>{
    res.send("put name")
})

app.disable("x-powered-by")
app.listen(process.env.EXPRESS_PORT, async () => {
   console.log(`App listening on port ${process.env.EXPRESS_PORT}!`)
})
